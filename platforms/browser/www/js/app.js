'use strict';

angular.module('lawyerApp',['ui.router','ngResource','lawyerApp.controllers','lawyerApp.services','lumx']);

angular.module('lawyerApp')
.config(function($stateProvider,$httpProvider){
    $stateProvider
    .state('lawyers',{
        url:'/lawyers',
        templateUrl:'partials/lawyers.html',
        controller:'LawyerListController'
    })
    .state('viewMovie',{
       url:'/lawyers/:id/view',
       templateUrl:'partials/lawyer-view.html',
       controller:'LawyerViewController'
    })
    .state('newMovie',{
        url:'/lawyers/new',
        templateUrl:'partials/lawyer-add.html',
        controller:'LawyerCreateController'
    })
    .state('editMovie',{
        url:'/lawyers/:id/edit',
        templateUrl:'partials/lawyer-edit.html',
        controller:'LawyerCreateController'
    });
})
.run(function($state,$rootScope){
   $rootScope.$state = $state;
   $state.go('lawyers');
});