'use strict';

angular.module('lawyerApp.services',[])
.factory('LawyerResource',function($resource){
    "use strict";
    return $resource('http://dev.nursoft.cl:3000/api/v1/lawyers/:id',{id:'@id'},
    {
        query:  {method:'GET', params:{id:''}, isArray:false},
        post:   {method:'POST'},
        update: {method:'PUT', params: {id:'@id'}},
        remove: {method:'DELETE'} 
    });
})

.service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
});