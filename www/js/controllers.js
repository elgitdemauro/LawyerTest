'use strict';

angular.module('lawyerApp.controllers',[])
.controller('LawyerListController',function($scope,$state,$stateParams,popupService,$window,LawyerResource,$location, $http){
    
    /* loader */
    $scope.loading = true;
    $http.get('http://dev.nursoft.cl:3000/api/v1/lawyers')
      .success(function(data) {
        
        $scope.loading = false;
        /* end loader */

        $scope.movies = LawyerResource.query();
        $scope.deleteLawyer = function(movie){
            if(popupService.showPopup('Really delete this Lawyer?')){
                LawyerResource.delete({id:movie.id},function(data){
                    $scope.movies = LawyerResource.query();
                });
            }
        }
        
        /* chevron toggle by order */
        $scope.setOrder = function (id) {
            $scope.id = id;
        };
    });
})

.controller('LawyerViewController',function($scope,$state,$stateParams,popupService,$window,LawyerResource){
    $scope.movie = LawyerResource.get({id:$stateParams.id});
    $scope.deleteLawyerView = function(movie){
        if(popupService.showPopup('Really delete this Lawyer?')){
            LawyerResource.delete({id:movie.id},function(data){
                console.log(data);
                $state.go('lawyers');
            });
        }
    }
})

.controller('LawyerCreateController',function($scope,$state,$stateParams,LawyerResource){
    $scope.title_view = 'Add Lawyer';
    $scope.movie = new LawyerResource();
    $scope.addLawyer = function(){
        $scope.movie.$save({id:$stateParams.id},function(){
            $state.go('lawyers');
        });
    }
    $scope.updateLawyer = function(){
        $scope.movie.$update({id:$stateParams.id},function(){
            $state.go('lawyers');
        });
    }
})

.controller('LawyerEditController',function($scope,$state,$stateParams,LawyerResource){
    $scope.title_view = 'Edit Lawyer';
    $scope.updateLawyer = function(){
        
        LawyerResource.$update({id:$stateParams.id, data:$scope.movie}, function(data){
            console.log(data);
            $state.go('lawyers');
        });
    };
    $scope.loadLawyer = function(){
        LawyerResource = LawyerResource.get({id:$stateParams.id});
    };
    $scope.loadLawyer();
});



